export default {
  Query: {
    hello: () => 'world',
    currentTime: () => new Date(),
    users: (_, __, { prisma }) => prisma.users(),
  },
};
