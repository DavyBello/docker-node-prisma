import { allow } from 'graphql-shield';

export default {
  Query: {
    currentTime: allow,
    hello: allow,
    users: allow,
  },
  Mutation: {},
  // Subscription: {},
  User: allow,
};
