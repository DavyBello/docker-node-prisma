import { gql } from 'apollo-server';

// The GraphQL schema
export default gql`
  scalar DateTime

  type Query {
    "A simple type for getting started!"
    hello: String
    currentTime: DateTime
    users: [User!]
  }

  type User {
    firstName: String
    lastName: String
    email: String
    phone: String
  }

  type Mutation {
    _empty: String
  }

  # type Subscription {
  #   _empty: String
  # }
`;
