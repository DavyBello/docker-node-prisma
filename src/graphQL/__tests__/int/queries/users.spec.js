import { createTestClient } from 'apollo-server-testing';
import { apolloServer } from '../../../../apolloServer';

const USERS_QUERY = `
{
    users {
        firstName
        lastName
        email
        phone
    }
}
`

describe('users Query', () => {
    test('should return users from the database', async () => {
        const { query } = createTestClient(apolloServer);

        expect(await query({ query: USERS_QUERY })).toMatchSnapshot();
    });
});
