import { createTestClient } from 'apollo-server-testing';
import { apolloServer } from '../../../../apolloServer';

const TIME_QUERY = `
{
    currentTime
}
`

describe('currentTime Query', () => {
    test('should return the date', async () => {
        const { query } = createTestClient(apolloServer);

        expect(await query({ query: TIME_QUERY })).toMatchSnapshot({
            data: {
                currentTime: expect.any(Date),
            }
        });
    });
});
