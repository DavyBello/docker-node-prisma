import { createTestClient } from 'apollo-server-testing';
import { apolloServer } from '../../../../apolloServer';

const HELLO_QUERY = `
{
    hello
}
`

describe('Hello Query', () => {
    test('should return string "world"', async () => {
        const { query } = createTestClient(apolloServer);

        expect(await query({ query: HELLO_QUERY })).toMatchSnapshot();
    });
});
