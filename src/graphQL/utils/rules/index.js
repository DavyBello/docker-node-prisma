import { rule } from 'graphql-shield';

export const isAuthenticated = rule()(async (parent, args, ctx) => {
  const user = await ctx.getUser();
  ctx.user = user;
  return user !== null;
});
