import { prisma } from '../../generated/prisma-client';

/**
 *
 *
 * @param {*} [{ req, res, connection: { context = {} } = {} }={}]
 * @returns
 */
const getContext = async ({ req, res } = {}) => {
  return {
    prisma,
    req,
    res,
  };
};

export default getContext;
