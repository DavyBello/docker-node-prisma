import getContext from '../getContext';
import { prisma } from '../../../generated/prisma-client';

describe('getContext', () => {
  test('should generate graphQL Context', async () => {
    expect(await getContext()).toEqual({ prisma });
  });
});
