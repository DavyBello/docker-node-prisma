import coreTypeDefs from './core/types';
import coreResolvers from './core/resolvers';
import corePermissions from './core/permissions';

export const typeDefsArray = [coreTypeDefs];
export const resolversArray = [coreResolvers];
export const permissions = corePermissions;
