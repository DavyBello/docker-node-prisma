import { mergeResolvers, mergeTypes } from 'merge-graphql-schemas';
import {
  typeDefsArray,
  resolversArray,
} from './index';

export { permissions } from './index';
export const typeDefs = mergeTypes(typeDefsArray, { all: true });
export const resolvers = mergeResolvers(resolversArray);
