import express from 'express';
import morgan from 'morgan';

import { prisma } from './generated/prisma-client';
import { apolloServer } from './apolloServer';

const app = express();

app.use(morgan('tiny'));

app.get('/', async (req, res) => {
  const users = await prisma.users();
  res.send(`
    <p>Hello Docker Node</p>
    <p>If you can see three user objects below then everything is working as it should:</p>
    <div>${JSON.stringify(users)}</div>
    `);
});

apolloServer.applyMiddleware({ app });

export default app;
