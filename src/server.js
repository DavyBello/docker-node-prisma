import app from './expressApp';

const PORT = 4000;

const dashes = '\n------------------------------------------------\n';

app.listen(PORT, () => {
    console.log(`${dashes}Docker Node is listening on port ${PORT}...${dashes}`);
});