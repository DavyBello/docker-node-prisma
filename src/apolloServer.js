import { ApolloServer, makeExecutableSchema } from 'apollo-server-express';
import { applyMiddleware } from 'graphql-middleware';
import { shield, deny } from 'graphql-shield';

import { typeDefs, resolvers, permissions } from './graphQL/schema';
import getContext from './graphQL/lib/getContext';

const shieldMiddleware = shield(permissions, {
  fallbackRule: deny,
});

export const schema = applyMiddleware(
  makeExecutableSchema({
    typeDefs,
    resolvers,
  }),
  shieldMiddleware,
);

export const apolloServer = new ApolloServer({
  schema,
  context: getContext,
});
