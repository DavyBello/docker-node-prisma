###############################################################################
# Step 1 : Builder image
#
FROM node:10-alpine AS builder

# Define working directory
WORKDIR /home/node/app

# Install dependencies
COPY package.json yarn.lock ./
RUN yarn install

# copy source and build babel
COPY . .
RUN yarn build

###############################################################################
# Step 2 : Run image
#
FROM node:10-alpine
WORKDIR /home/node/app

# Install Node.js dependencies
RUN yarn global add prisma
COPY package.json yarn.lock ./
RUN yarn install --no-cache --frozen-lockfile --production;

# Copy builded source from the upper builder stage
COPY --from=builder /home/node/app/build ./build

# Copy other files
COPY --from=builder /home/node/app/prisma ./prisma

CMD sh -c "yarn prisma:deploy && yarn start"